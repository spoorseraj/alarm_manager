
package sa.app.alarmmanager.models;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class SmsModel {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("at_time")
    private String mAtTime;
    @SerializedName("code")
    private String mCode;
    @SerializedName("folderName")
    private String mFolderName;
    @SerializedName("id")
    private String mId;
    @SerializedName("insert_time")
    private String mInsertTime;
    @SerializedName("msg")
    private String mMsg;
    @SerializedName("readState")
    private String mReadState;
    @SerializedName("user_id")
    private String mUserId;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAtTime() {
        return mAtTime;
    }

    public void setAtTime(String atTime) {
        mAtTime = atTime;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getFolderName() {
        return mFolderName;
    }

    public void setFolderName(String folderName) {
        mFolderName = folderName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInsertTime() {
        return mInsertTime;
    }

    public void setInsertTime(String insertTime) {
        mInsertTime = insertTime;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public String getReadState() {
        return mReadState;
    }

    public void setReadState(String readState) {
        mReadState = readState;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

}
