package sa.app.alarmmanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import sa.app.alarmmanager.utils.BaseApplication;

public class AlarmReceiver extends BroadcastReceiver {
    private static final String TAG = "alarm_";
    int hr,min,sec;
    Context ctx;
    @Override
    public void onReceive(Context context, Intent intent) {
        ctx=context;

        hr = Integer.parseInt(BaseApplication.getData("hour",""));
        min = Integer.parseInt(BaseApplication.getData("min",""));
        sec = Integer.parseInt(BaseApplication.getData("sec",""));

        Calendar cur_cal = Calendar.getInstance();
        Calendar curent = Calendar.getInstance();
        cur_cal.set(Calendar.HOUR_OF_DAY, hr);
        cur_cal.set(Calendar.MINUTE, min);
        cur_cal.set(Calendar.SECOND, sec);

        if (curent.compareTo(cur_cal) <= 0) {
            Log.d(TAG, "onReceive: "+BaseApplication.getData("hour",""));

            Intent i = new Intent(context, AlarmIntentService.class);
            PendingIntent pintent = PendingIntent.getService(context, 0, i, 0);
            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarm.set(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis()+2000, pintent);


        }

    }


}
