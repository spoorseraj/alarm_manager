package sa.app.alarmmanager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import javax.inject.Inject;

import sa.app.alarmmanager.getSms.ShowSmsActivity;
import sa.app.alarmmanager.models.Sms;
import sa.app.alarmmanager.utils.BaseApplication;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "version";
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    Activity mActivity = this;
    EditText hourText, minText, secText;
    Button startAlarm;
    List<Sms> lst;

    int hr = 0;
    int min = 0;
    int sec = 0;
    int result = 1;
    @Inject
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        loadDataSchedule();

        startAlarm.setOnClickListener(v -> {
            scheduleAlarm();
        });
        //cancel alarm
        findViewById(R.id.stopAlarm).setOnClickListener(v -> {
            Intent intent = new Intent(this, AlarmIntentService.class);
            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            //delete stored time
            BaseApplication.deleteData("hour");
            BaseApplication.deleteData("min");
            BaseApplication.deleteData("sec");

            hourText.setText("");
            minText.setText("");
            secText.setText("");

        });
        BaseApplication.getMyComponent().inject(this);
    }

    private void loadDataSchedule() {
        hourText.setText(BaseApplication.getData("hour", ""));
        minText.setText(BaseApplication.getData("min", ""));
        secText.setText(BaseApplication.getData("sec", ""));

    }

    private void bind() {
        hourText = findViewById(R.id.hour);
        minText = findViewById(R.id.min);
        secText = findViewById(R.id.sec);
        startAlarm = findViewById(R.id.startAlarm);

    }

    private void scheduleAlarm() {
        String shr = hourText.getText().toString();
        String smin = minText.getText().toString();
        String ssec = secText.getText().toString();
        Boolean flag = validate(shr, smin, ssec);
        if (flag) startAlarm.setTextColor(Color.rgb(100, 100, 255));
        else startAlarm.setTextColor(Color.rgb(255, 0, 0));
//        -----------------------------------------------------
        BaseApplication.setData("hour", shr.equals("") ? "0" : shr);
        BaseApplication.setData("min", smin.equals("") ? "0" : smin);
        BaseApplication.setData("sec", ssec.equals("") ? "0" : ssec);

        Intent intent = new Intent(context, AlarmReceiver.class);
        sendBroadcast(intent);

    }

    private Boolean validate(String shr, String smin, String ssec) {
        Boolean flag = true;
        if (shr.equals("") && smin.equals("") && ssec.equals(""))
            flag = false;
        else {
            if (shr.equals(""))
                hr = 0;
            else {
                hr = Integer.parseInt(hourText.getText().toString());
                if (hr > 24) {
                    flag = false;
                    hourText.setText("");
                }
            }
            if (smin.equals(""))
                min = 0;
            else {
                min = Integer.parseInt(minText.getText().toString());
                if (min > 59) {
                    flag = false;
                    minText.setText("");
                }
            }

            if (ssec.equals(""))
                sec = 0;
            else {
                sec = Integer.parseInt(secText.getText().toString());
                if (sec > 59) {
                    flag = false;
                    secText.setText("");
                }
            }
        }
        return flag;
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.sms) {
            startActivity(new Intent(this, ShowSmsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
