package sa.app.alarmmanager;

import java.util.List;


import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import sa.app.alarmmanager.models.ResultModel;
import sa.app.alarmmanager.models.SmsModel;


public interface AlarmInterface {

    @GET("getSmsList.php/")
    Observable<List<SmsModel>> load(@Query("token") String token);

    @FormUrlEncoded
    @POST("insertSms.php")
    Observable<ResultModel>  getSms(@Field("token") String token,
                                    @Field("code") String code,
                                    @Field("address") String address,
                                    @Field("msg") String msg,
                                    @Field("readState") String readState,
                                    @Field("time") String time,
                                    @Field("folderName") String folderName);

}
