package sa.app.alarmmanager.utils;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

import sa.app.alarmmanager.Dagger.DaggerMyComponent;
import sa.app.alarmmanager.Dagger.MyComponent;
import sa.app.alarmmanager.Dagger.MyModule;
import sa.app.alarmmanager.MainActivity;

public class BaseApplication extends Application {
    public BaseApplication context;
    private static MyComponent myComponent;

    public static MyComponent getMyComponent() {
        return myComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        myComponent = DaggerMyComponent.builder().myModule(new MyModule(this)).build();

        Hawk.init(context).build();

    }

    public static String getData(String key, String defaultValue) {
        return Hawk.get(key, defaultValue);
    }

    public static void setData(String key, String value) {
        Hawk.put(key, value);
    }

    public static void deleteData(String key) {
        Hawk.delete(key);
    }

}
