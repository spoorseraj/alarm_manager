package sa.app.alarmmanager.Dagger;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import sa.app.alarmmanager.AlarmInterface;
import sa.app.alarmmanager.AlarmService;
import sa.app.alarmmanager.getSms.Contract;
import sa.app.alarmmanager.getSms.Model;
import sa.app.alarmmanager.getSms.Presenter;
import sa.app.alarmmanager.utils.RxRetroGenerator;

@dagger.Module
public class MyModule {
    Context mContext;

    public MyModule(Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    public Context getContext() {
        return mContext;
    }

    @Provides
    Contract.Model getModel() {
        return new Model();
    }

    @Provides
    Contract.Presenter getPresenter() {
        return new Presenter();
    }


    @Provides
    AlarmInterface getInterface() {
        return RxRetroGenerator.createService(AlarmInterface.class);
    }
}
