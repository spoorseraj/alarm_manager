package sa.app.alarmmanager.Dagger;

import dagger.Component;
import sa.app.alarmmanager.MainActivity;

import sa.app.alarmmanager.getSms.Model;
import sa.app.alarmmanager.getSms.Presenter;
import sa.app.alarmmanager.getSms.ShowSmsActivity;

@Component(modules = {MyModule.class})
public interface MyComponent {
    void inject(MainActivity mainActivity);
    void inject(ShowSmsActivity showSmsActivity);
    void inject(Presenter presenter);
    void inject(Model model);


}
