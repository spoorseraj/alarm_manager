package sa.app.alarmmanager.getSms;

import android.content.Context;
import android.os.RecoverySystem;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sa.app.alarmmanager.R;
import sa.app.alarmmanager.models.SmsModel;

public class SmsAdapter extends RecyclerView.Adapter<SmsAdapter.Holder> {
    Context context;
    List<SmsModel> smsList;

    public SmsAdapter(Context context, List<SmsModel> smsList) {
        this.context = context;
        this.smsList = smsList;
    }

    @NonNull
    @Override
    public SmsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(context).inflate(R.layout.sms_item,viewGroup,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SmsAdapter.Holder holder, int i) {
        holder.addres.setText(smsList.get(i).getAddress());
        holder.msg.setText(smsList.get(i).getMsg());
    }

    @Override
    public int getItemCount() {
        return smsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView addres,msg;
        public Holder(@NonNull View itemView) {
            super(itemView);
            addres=itemView.findViewById(R.id.address);
            msg = itemView.findViewById(R.id.msg);
        }
    }
}
