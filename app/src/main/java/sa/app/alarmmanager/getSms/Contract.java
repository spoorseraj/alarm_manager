package sa.app.alarmmanager.getSms;

import java.util.List;

import sa.app.alarmmanager.models.SmsModel;

public interface Contract {
    interface View{
        void onSuccessLoad(List<SmsModel> list);
        void showMessage(String msg);
    }
    interface Presenter{
        void attachView(View view);
        void onLoadingData();
        void onSuccessLoading(List<SmsModel> list);
        void onMessage(String msg);
    }
    interface Model{
        void attachPresenter(Presenter presenter);
        void loadData();

    }
}
