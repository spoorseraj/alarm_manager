package sa.app.alarmmanager.getSms;

import java.util.List;
import java.util.Observable;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import sa.app.alarmmanager.AlarmInterface;
import sa.app.alarmmanager.models.SmsModel;
import sa.app.alarmmanager.utils.BaseApplication;

public class Model implements Contract.Model {
    private Contract.Presenter presenter;
    @Inject
    AlarmInterface alarmInterface;

    @Override
    public void attachPresenter(Contract.Presenter presenter) {

        this.presenter = presenter;
        BaseApplication.getMyComponent().inject(this);
    }

    @Override
    public void loadData() {
        String token=BaseApplication.getData("token","");
        alarmInterface.load(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess,this::onfail);
    }

    private void onfail(Throwable throwable) {
        presenter.onMessage(throwable.getMessage());
    }

    private void onSuccess(List<SmsModel> smsModels) {
        presenter.onSuccessLoading(smsModels);
    }
}
