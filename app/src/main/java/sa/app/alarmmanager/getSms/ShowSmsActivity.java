package sa.app.alarmmanager.getSms;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import java.util.List;

import javax.inject.Inject;

import sa.app.alarmmanager.AlarmInterface;
import sa.app.alarmmanager.R;
import sa.app.alarmmanager.models.SmsModel;
import sa.app.alarmmanager.utils.BaseApplication;

public class ShowSmsActivity extends AppCompatActivity implements Contract.View {
    @Inject
    Contract.Presenter presenter;
    RecyclerView recycler;
    LottieAnimationView lottie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_sms);

        recycler=findViewById(R.id.recycle);
        lottie=findViewById(R.id.lottie);

        BaseApplication.getMyComponent().inject(this);
        presenter.attachView(this);
        lottie.setVisibility(View.VISIBLE);
        presenter.onLoadingData();



    }

    @Override
    public void onSuccessLoad(List<SmsModel> list) {
        if(list.size()==0) Toast.makeText(this, "this is empty list", Toast.LENGTH_SHORT).show();
        lottie.setVisibility(View.INVISIBLE);
        SmsAdapter smsAdapter=new SmsAdapter(this,list);
        recycler.setAdapter(smsAdapter);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
