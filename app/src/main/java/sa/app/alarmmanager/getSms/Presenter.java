package sa.app.alarmmanager.getSms;

import java.util.List;

import javax.inject.Inject;

import sa.app.alarmmanager.AlarmInterface;
import sa.app.alarmmanager.models.SmsModel;
import sa.app.alarmmanager.utils.BaseApplication;

public class Presenter implements Contract.Presenter {
    @Inject
    Contract.Model model;
    @Inject
    AlarmInterface alarmInterface;

    private Contract.View view;

    @Override
    public void attachView(Contract.View view) {
        BaseApplication.getMyComponent().inject(this);
        this.view = view;
        model.attachPresenter(this);

    }

    @Override
    public void onLoadingData() {
        model.loadData();
    }

    @Override
    public void onSuccessLoading(List<SmsModel> list) {
        view.onSuccessLoad(list);
    }

    @Override
    public void onMessage(String msg) {
        view.showMessage(msg);
    }

}
